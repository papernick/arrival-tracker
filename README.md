# 'Employee Arrival Tracker' Coding Exercise

## Dependencies

- .NET Core 5

## Installation

1. `dotnet restore`

2. `dotnet tool restore`

3. `dotnet ef database update`

## Running

1. `dotnet run`

2. Navigate to [http://localhost:5000/swagger](http://localhost:5000/swagger) and interact with the API
