using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

using Consumer.Arrivals.Models;

namespace Consumer.WebServiceMock
{
    [ApiController]
    [Route("[controller]")]
    public class WebServiceController : ControllerBase
    {
        [HttpPost]
        [Route("subscribe")]
        public async Task<ActionResult<SubscriptionResponse>> Subscribe(
            [FromQuery] DateTime date,
            [FromQuery] string callback)
        {
            Console.WriteLine($"Web service called with: 'date': {date}; 'callback': {callback}'");

            var response = await Task.FromResult(new SubscriptionResponse
            {
                Token = "00d963bd61094d57a8b521bc100ed3c1",
                Expires = DateTime.UtcNow.AddHours(3),
            });

            return Ok(response);
        }
    }
}
