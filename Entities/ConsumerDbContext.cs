using Microsoft.EntityFrameworkCore;

using Consumer.Entities.Models;

namespace Consumer.Entities
{
    public class ConsumerDbContext : DbContext
    {
        public DbSet<Arrival> Arrivals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=arrivals.db");
    }
}
