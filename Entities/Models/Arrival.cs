using System;

namespace Consumer.Entities.Models
{
    public class Arrival
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public DateTime ArrivalTime { get; set; }
    }
}
