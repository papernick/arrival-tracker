using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

using Consumer.Arrivals.Models;
using Consumer.Arrivals.Exceptions;

#nullable enable

namespace Consumer.Arrivals.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IConfiguration config;

        private readonly ILogger logger;

        private readonly IHttpClientFactory httpClientFactory;

        public SubscriptionService(
            IConfiguration config,
            ILogger<SubscriptionService> logger,
            IHttpClientFactory httpClientFactory)
        {
            this.config = config;
            this.logger = logger;
            this.httpClientFactory = httpClientFactory;
        }

        public async Task<SubscriptionResponse?> Subscribe(DateTime date)
        {
            var queryParams = new Dictionary<string, string?>()
            {
                { "date", string.Format("{0:yyyy-MM-dd}", date) },
                { "callback", this.config["Arrivals:ReceiveUrl"] },
            };
            var requestUri = QueryHelpers.AddQueryString(this.config["WebService:SubscribeUrl"], queryParams);
            var request = new HttpRequestMessage(HttpMethod.Post, requestUri);
            request.Headers.Add("Accept", "application/json");
            request.Headers.Add("Accept-Client", "Fourth-Monitor");

            HttpClient httpClient = this.httpClientFactory.CreateClient();
            HttpResponseMessage response = await httpClient.SendAsync(request);
            string content = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                this.logger.LogError($"Error subscribing to WebService. Response: {content}");
                throw new SubscriptionException("Oh noes, response not 200");
            }

            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };

            return JsonSerializer.Deserialize<SubscriptionResponse>(content, options);
        }
    }
}
