using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Consumer.Arrivals.Exceptions;
using Consumer.Arrivals.Models;
using Consumer.Entities;
using Consumer.Entities.Models;

#nullable enable

namespace Consumer.Arrivals.Services
{
    public class ArrivalService : IArrivalService
    {
        private readonly ConsumerDbContext dbContext;

        private readonly ILogger logger;

        public ArrivalService(ConsumerDbContext dbContext, ILogger<ArrivalService> logger)
        {
            this.dbContext = dbContext;
            this.logger = logger;
        }

        public async Task<bool> SaveArrival(int employeeId, DateTime time)
        {
            var arrival = new Arrival { EmployeeId = employeeId, ArrivalTime = time };

            this.dbContext.Add(arrival);

            try
            {
                Console.WriteLine($"Saving employeeId={employeeId} arrival on time={time}");
                await this.dbContext.SaveChangesAsync();
            }
            catch (DbUpdateException exc)
            {
                string error = $"Could not save arrival for Employee={employeeId}";
                this.logger.LogError(exc, error);
                throw new ArrivalSaveException(error);
            }

            return true;
        }

        public async Task<IEnumerable<ArrivalListItemModel>> ListArrivals(
            int page,
            int pageSize,
            DateTime? startDate,
            DateTime? endDate)
        {
            if (page <= 0 || pageSize <= 0)
            {
                return new ArrivalListItemModel[0];
            }

            IQueryable<Arrival> query = this.dbContext.Arrivals;

            if (startDate != null)
            {
                query = query.Where(ar => ar.ArrivalTime >= startDate);
            }
            if (endDate != null)
            {
                query = query.Where(ar => ar.ArrivalTime <= endDate);
            }

            return await query
                .OrderByDescending(ar => ar.ArrivalTime)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Select(ar => new ArrivalListItemModel
                {
                    EmployeeId = ar.EmployeeId,
                    When = ar.ArrivalTime,
                })
                .ToListAsync();
        }
    }
}
