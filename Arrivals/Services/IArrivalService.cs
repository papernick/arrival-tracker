using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Consumer.Arrivals.Models;

#nullable enable

namespace Consumer.Arrivals.Services
{
    public interface IArrivalService
    {
        Task<bool> SaveArrival(int employeeId, DateTime time);

        Task<IEnumerable<ArrivalListItemModel>> ListArrivals(
            int page,
            int pageSize,
            DateTime? startDate,
            DateTime? endDate);
    }
}
