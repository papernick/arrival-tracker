using System;
using System.Threading.Tasks;

using Consumer.Arrivals.Models;

#nullable enable

namespace Consumer.Arrivals.Services
{
    public interface ISubscriptionService
    {
        Task<SubscriptionResponse?> Subscribe(DateTime date);
    }
}
