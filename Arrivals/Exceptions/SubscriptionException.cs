using System;

namespace Consumer.Arrivals.Exceptions
{
    public class SubscriptionException : Exception
    {
        public SubscriptionException()
        {
        }

        public SubscriptionException(string message)
            : base(message)
        {
        }

        public SubscriptionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
