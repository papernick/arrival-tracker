using System;

namespace Consumer.Arrivals.Exceptions
{
    public class ArrivalSaveException : Exception
    {
        public ArrivalSaveException()
        {
        }

        public ArrivalSaveException(string message)
            : base(message)
        {
        }

        public ArrivalSaveException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
