using System;

namespace Consumer.Arrivals.Models
{
    public class ArrivalListItemModel
    {

        public int EmployeeId { get; set; }

        public DateTime When { get; set; }
    }
}
