using System;
using System.ComponentModel.DataAnnotations;

namespace Consumer.Arrivals.Models
{
    public class ArrivalRequestModel
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public DateTime When { get; set; }
    }
}
