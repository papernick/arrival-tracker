using System;

namespace Consumer.Arrivals.Models
{
    public class SubscriptionResponse
    {
        public string Token { get; set; }

        public DateTime Expires { get; set; }
    }
}
