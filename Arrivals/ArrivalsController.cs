﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Consumer.Arrivals.Exceptions;
using Consumer.Arrivals.Models;
using Consumer.Arrivals.Services;

#nullable enable

namespace Consumer.Arrivals
{
    [ApiController]
    [Route("[controller]")]
    public class ArrivalsController : ControllerBase
    {
        private readonly ISubscriptionService subscriptionService;

        private readonly IArrivalService arrivalService;

        public ArrivalsController(
            ISubscriptionService subscriptionService,
            IArrivalService arrivalService)
        {
            this.subscriptionService = subscriptionService;
            this.arrivalService = arrivalService;
        }

        [HttpPost]
        [Route("subscribe")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<SubscriptionResponse?>> Subscribe()
        {
            try
            {
                SubscriptionResponse? model =
                    await this.subscriptionService.Subscribe(new DateTime(year: 2016, month:3, day: 10));

                return Ok(model);
            }
            catch (SubscriptionException exc)
            {
                return BadRequest(new { error = exc.Message });
            }
        }

        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="startDate">Format: YYYY-MM-DD</param>
        /// <param name="endDate">Format: YYYY-MM-DD</param>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<ArrivalListItemModel>>> ListArrivals(
            [FromQuery] int page = 1,
            [FromQuery, Range(1, 100)] int pageSize = 50,
            [FromQuery] DateTime? startDate = null,
            [FromQuery] DateTime? endDate = null)
        {
            IEnumerable<ArrivalListItemModel> arrivals =
                await this.arrivalService.ListArrivals(page, pageSize, startDate, endDate);

            return Ok(arrivals);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> SaveArrival(ArrivalRequestModel model)
        {
            try
            {
                bool isSaved = await this.arrivalService.SaveArrival(model.EmployeeId, model.When);
                if (!isSaved)
                {
                    return BadRequest(new { error = "Could not save arrival." });
                }

                return NoContent();
            }
            catch (ArrivalSaveException exc)
            {
                return BadRequest(new { error = exc.Message });
            }
        }
    }
}
